package se.kth.md.adaptors.basicexample;

import net.oauth.OAuthException;
import org.apache.log4j.Level;
import org.apache.wink.client.ClientResponse;
import org.eclipse.lyo.client.exception.ResourceNotFoundException;
import org.eclipse.lyo.client.oslc.OSLCConstants;
import org.eclipse.lyo.client.oslc.OslcClient;
import org.eclipse.lyo.client.oslc.resources.OslcQuery;
import org.eclipse.lyo.client.oslc.resources.OslcQueryParameters;
import org.eclipse.lyo.client.oslc.resources.OslcQueryResult;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.adaptors.basicexample.resources.Developer;
import se.kth.md.adaptors.basicexample.resources.SoftwareProduct;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class BasicClientMain {

    // Service Provider Catalog
    static String serviceProviderCatalogURL = "http://localhost:8080/BasicAdaptor/services/catalog/singleton";

    // URL related to the Basic Domain
    static String basicDomainSPTitle = "Basic Domain Service Provider: (/BDSP)";

    static String developerURI = "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP/resources/developers/1";
    static String queryDevelopersURL = "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP/resources/developers";
    static String creationDevelopersURL = "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP/resources/CreationFactoryDevelopers";

    static String queryProductsURL = "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP/resources/softwareproducts";
    static String creationProductsURL = "http://localhost:8080/BasicAdaptor/services/bd-services/BDSP/resources/CreationFactorySoftwareProducts";

    private static final Logger log = LoggerFactory.getLogger(BasicClientMain.class);

    public static void main(String[] args) throws Exception {

        // Because of too verbose warnings from a wink library class
        // (logging based on Log4j)
        org.apache.log4j.Logger.getLogger("org.apache.wink.common.internal.registry.metadata.ProviderMetadataCollector").setLevel(Level.OFF);

        OslcClient client = new OslcClient();

        testGetServiceProviderCatalog(client);

        testLookupQueryCapability(client);

        testGetDeveloper(client);

        testOslcQuery(client, queryDevelopersURL, Developer.class);

        testCreateDeveloper(client, "Snow White");

        testUpdate(client);

        // Not implemented on the server side
        testOslcQueryWhereClause(client, queryDevelopersURL, Developer.class);
    }

    public static void testGetServiceProviderCatalog (OslcClient client) {
        try {
            ClientResponse clientResource = client.getResource(serviceProviderCatalogURL, OSLCConstants.CT_RDF);

            ServiceProviderCatalog serviceProviderCatalog = clientResource.getEntity(ServiceProviderCatalog.class);

            log.info(" Service Provider Catalog URI retrieved: {}", serviceProviderCatalog.getAbout());

        } catch (IOException | OAuthException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void testLookupQueryCapability (OslcClient client) throws URISyntaxException, OAuthException, ResourceNotFoundException, IOException {
        String serviceProviderUrl = client.lookupServiceProviderUrl(serviceProviderCatalogURL,
                basicDomainSPTitle);

        String softwareProductQueryCapability = client.lookupQueryCapability(serviceProviderUrl,
                BasicClientConstants.BASICDOMAIN_DOMAIN,
                BasicClientConstants.TYPE_SOFTWAREPRODUCT);

        log.info("QueryCapability retrieved: {}", softwareProductQueryCapability);
    }

    public static Developer testGetDeveloper (OslcClient client) throws OAuthException, IOException, URISyntaxException {
        ClientResponse response = client.getResource(developerURI, OSLCConstants.CT_RDF);
        Developer resource = null;
        if (!response.getMessage().equals("Not Found")) {
            resource = response.getEntity(Developer.class);
            log.info("Developer URI retrieved: {}", resource.getAbout());
        } else {
            System.out.println(developerURI + " Not Found");
        }
        return resource;
    }

    public static List<AbstractResource> testOslcQuery (OslcClient client, String queryURL, Class clazz) throws IOException {
        OslcQueryParameters queryParams = new OslcQueryParameters();

        OslcQuery query = new OslcQuery(client, queryURL);

        OslcQueryResult result = query.submit();

        List<AbstractResource> resourceList = new ArrayList<>();

        boolean processAsJavaObjects = true;
        processPagedQueryResults(result, client, processAsJavaObjects, clazz, resourceList);

        return resourceList;
    }


    private static void processPagedQueryResults(OslcQueryResult result, OslcClient client, boolean asJavaObjects,
                                                 Class resourceClass,
                                                 List<AbstractResource> resourceList) {
        int page = 1;
        do {
            System.out.println("\nPage " + page + ":\n");
            processCurrentPage(result,client,asJavaObjects, resourceClass, resourceList);
            if (result.hasNext()) {
                result = result.next();
                page++;
            } else {
                break;
            }
        } while(true);
    }

    private static void processCurrentPage(OslcQueryResult result, OslcClient client, boolean asJavaObjects,
                                           Class resourceClass,
                                           List<AbstractResource> resourceList) {

        for (String resultsUrl : result.getMembersUrls()) {
            System.out.println(resultsUrl);

            ClientResponse response = null;
            try {

                //Get a single artifact by its URL
                response = client.getResource(resultsUrl, OSLCConstants.CT_RDF);

                if (response != null) {
                    //De-serialize it as a Java object
                    if (asJavaObjects) {
                        //ChangeRequest cr = response.getEntity(ChangeRequest.class);
                        //printChangeRequestInfo(cr);   //print a few attributes
                        AbstractResource aR = (AbstractResource)response.getEntity(resourceClass);
                        System.out.println("# " + aR.getAbout());
                        resourceList.add(aR);
                    } else {
                        //Just print the raw RDF/XML (or process the XML as desired)
                        // result.getRdfModel().write(System.out, "RDF/XML", null);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static Developer testCreateDeveloper (OslcClient client, String devName) throws IOException, URISyntaxException, OAuthException {
        Developer newDeveloper = new Developer();
        newDeveloper.setTitle(devName);

        // Apparently, the URI has to be set to be processed on the server side,
        // even if it is actually under the responsibility of the service provider
        // to create it.
        newDeveloper.setAbout(URI.create("http://test8999.com/29"));

        ClientResponse rawResponse = client.createResource(creationDevelopersURL, newDeveloper, OSLCConstants.CT_RDF);

        // For debug purpose
        // Caution though since a call to getEntity consumes the content of the http
        // response, and it cannot be called a 2nd time without raising an exception
        //String responseAsString = rawResponse.getEntity(String.class);

        int statusCode = rawResponse.getStatusCode();
        Developer returnedResource = rawResponse.getEntity(Developer.class);
        System.out.println("Status code for POST of new artifact: " + statusCode);

        return returnedResource;
    }

    public static void testUpdate (OslcClient client) throws IOException, OAuthException, URISyntaxException {
        List<SoftwareProduct> listProducts = (List<SoftwareProduct>)(Object) testOslcQuery(client, queryProductsURL, SoftwareProduct.class);
        Developer newDeveloper = testCreateDeveloper(client, "The 7th Dwarf");

        HashSet<Link> updatedLinks = new HashSet<>();
        for (SoftwareProduct p : listProducts) {
            updatedLinks.add(new Link(p.getAbout()));
        }
        newDeveloper.setDevelops(updatedLinks);

        ClientResponse rawResponse = client.updateResource(newDeveloper.getAbout().toString(), newDeveloper, OSLCConstants.CT_RDF);

        //Model rdfModel = ModelFactory.createDefaultModel();
        //rdfModel.read(rawResponse.getEntity(InputStream.class), newDeveloper.getAbout().toString());

        //String responseAsString = rawResponse.getEntity(String.class);
//        Developer returnedResource = rawResponse.getEntity(Developer.class);

        System.out.println("Status code for PUT of updated artifact: " + rawResponse.getStatusCode());

    }

    // Not implemented on the server side
    public static void testOslcQueryWhereClause (OslcClient client, String queryURL, Class clazz) throws IOException {
        OslcQueryParameters queryParams = new OslcQueryParameters();
        queryParams.setWhere("bex:DevName=\"Snow White\"");

        OslcQuery query = new OslcQuery(client, queryURL, queryParams);

        OslcQueryResult result = query.submit();

        boolean processAsJavaObjects = true;
        //processPagedQueryResults(result, client, processAsJavaObjects, clazz, resourceList);
    }

}
