package experiments.reification;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;

import java.net.URI;

public class ExtendedLink<T extends AbstractResource> extends Link {

    private T annotation;

    public ExtendedLink(URI resource) {
        super(resource);
    }

    public ExtendedLink(URI resource, String label, T annotation) {
        super(resource, label);
        this.annotation = annotation;
    }

    public T getAnnotation() {
        return annotation;
    }

    public void setAnnotation(T annotation) {
        if (getLabel().isEmpty()) {
            setLabel(annotation.getAbout().toString());
        } else if (!annotation.getAbout().equals(getLabel())) {
            // TODO: Raise an exception
        }
        this.annotation = annotation;
    }

}
