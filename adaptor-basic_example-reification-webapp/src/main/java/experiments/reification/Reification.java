package experiments.reification;

import com.hp.hpl.jena.rdf.model.*;
import org.eclipse.lyo.oslc4j.core.exception.OslcCoreApplicationException;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.provider.jena.JenaModelHelper;
import org.eclipse.lyo.tools.store.ModelUnmarshallingException;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.StoreFactory;
import se.kth.md.adaptors.basicexample.resources.Developer;
import se.kth.md.adaptors.basicexample.resources.ExtendedRequirement;
import se.kth.md.adaptors.basicexample.resources.SoftwareProduct;

import javax.xml.datatype.DatatypeConfigurationException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;

public class Reification {

    private static Model createModel () {
        String NS = "http://url.com/";
        Model model = ModelFactory.createDefaultModel();

        Resource requirement = model.createResource( NS+"requirement" );
        Property isImplementedBy = model.createProperty( NS+"isImplementedBy" );
        Resource product = model.createResource( NS+"product" );

        Statement stmt1 = model.createStatement(requirement, isImplementedBy, product);

        model.add(stmt1);

        //System.out.println();
        //model.write( System.out, "RDF/XML", null );
        //System.out.println();

        Property linkmetadata = model.createProperty(NS+"linkmetadata");
        Resource metadata = model.createResource(NS+"metadata3782");

        ReifiedStatement rstmt = stmt1.createReifiedStatement();

        model.add(rstmt, linkmetadata, metadata);

        Property createdBy = model.createProperty(NS+"createdBy");
        Resource creator = model.createResource(NS+"staff3022");

        Statement stmt2 = model.createStatement(metadata, createdBy, creator);

        model.add(stmt2);

        //model.write( System.out, "RDF/XML", null );

        return model;
    }

    private static Store buildStore() {
        Store store = null;
        store = StoreFactory.inMemory();
        //store = StoreFactory.sparql("http://fuseki.aide.md.kth.se/reification/sparql", "http://fuseki.aide.md.kth.se/reification/update");
        //UpdateProcessor updateProcessor = store.removeAll();
        //updateProcessor.execute();

        return store;
    }

    private static ExtendedInMemoryStoreImpl buildExtendedStore() {
        return new ExtendedInMemoryStoreImpl();
    }

    private static SoftwareProduct createOSLC4JModel (String NS) {
        SoftwareProduct softwareProduct = null;
        HashSet<Link> links;
        try {
            softwareProduct = new SoftwareProduct(URI.create(NS+"/product8932"));
            softwareProduct.setTitle("my product title");
            softwareProduct.setLineOfCode(3000);

            ExtendedRequirement r1 = new ExtendedRequirement(URI.create(NS+"/requirement1111"));
            r1.setTitle("my requirement title 1111");
            Date date = new Date();
            r1.setCreated(date);

            Developer d1 = new Developer(URI.create(NS+"/developer11"));
            d1.setDevName("Snow White");
            d1.addDevelops(new Link(softwareProduct.getAbout()));

            LinkAnnotation lAnnotation1 = new LinkAnnotation(URI.create(NS+"/linkannotation11"));
            lAnnotation1.setCreator(new Link(d1.getAbout()));
            lAnnotation1.setCreated(new Date());

            // Link towards r1 with annotation1
            ExtendedLink<LinkAnnotation> extLink1 = new ExtendedLink<LinkAnnotation>(r1.getAbout());
            extLink1.setLabel(lAnnotation1.getAbout().toString());
            extLink1.setAnnotation(lAnnotation1);
            softwareProduct.addRequirements(extLink1);

            ExtendedRequirement r2 = new ExtendedRequirement(URI.create(NS+"/requirement2222"));
            r1.setTitle("my requirement title 2222");
            date = new Date();
            r2.setCreated(date);

            Developer d2 = new Developer(URI.create(NS+"/developer22"));
            d2.setDevName("The Fifth Dwarf");
            d2.addDevelops(new Link(softwareProduct.getAbout()));

            LinkAnnotation lAnnotation2 = new LinkAnnotation(URI.create(NS+"/linkannotation22"));
            lAnnotation2.setCreator(new Link(d2.getAbout()));
            lAnnotation2.setCreated(new Date());

            // Link towards r2 with annotation2
            ExtendedLink<LinkAnnotation> extLink2 = new ExtendedLink<LinkAnnotation>(r2.getAbout());
            extLink2.setLabel(lAnnotation2.getAbout().toString());
            extLink2.setAnnotation(lAnnotation2);
            softwareProduct.addRequirements(extLink2);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return softwareProduct;
    }

    public static void main(String[] args) throws InvocationTargetException, DatatypeConfigurationException, OslcCoreApplicationException, IllegalAccessException, StoreAccessException, ModelUnmarshallingException {

        String NS = "http://test.reification.se";

        //Store store = buildStore();
        ExtendedInMemoryStoreImpl store = buildExtendedStore();

        //Model jenaModel = createModel();

        SoftwareProduct softwareProduct = createOSLC4JModel(NS);
        Object[] array = new Object[]{(Object)softwareProduct};
        final Model modelFromOSLC4J = JenaModelHelper.createJenaModel(array);

        System.out.println("RDF graph generated:");
        modelFromOSLC4J.write( System.out, "RDF/XML", null );
        System.out.println();

        //store.putResources(URI.create(NS), Arrays.asList(softwareProduct));
        store.putResources(URI.create(NS), Arrays.asList(softwareProduct), true);

        //SoftwareProduct softwareProductFromStore = store.getResourceUnderKey(URI.create(NS), softwareProduct.getAbout(), SoftwareProduct.class);
        SoftwareProduct softwareProductFromStore = store.getResourceUnderKey(URI.create(NS), softwareProduct.getAbout(), SoftwareProduct.class, true);
        Object[] arrayFromStore = new Object[]{(Object)softwareProductFromStore};
        final Model modelFromOSLC4JFromStore = JenaModelHelper.createJenaModel(arrayFromStore);

        System.out.println("RDF graph retrieved from store");
        modelFromOSLC4JFromStore.write( System.out, "RDF/XML", null );
        System.out.println();

    }

}
