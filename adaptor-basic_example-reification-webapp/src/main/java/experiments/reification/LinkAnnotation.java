package experiments.reification;

import org.eclipse.lyo.oslc4j.core.annotation.*;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.ValueType;
import se.kth.md.adaptors.basicexample.BasicAdaptorConstants;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

@OslcNamespace("http://annotation.org/nsp#")
@OslcName("LinkAnnotation")
@OslcResourceShape(title = "LinkAnnotation Resource Shape", describes = "http://annotation.org/nsp#LinkAnnotation")
public class LinkAnnotation extends AbstractResource implements ILinkAnnotation {

    private Link creator = new Link();
    private Date created;

    public LinkAnnotation ()
            throws URISyntaxException
    {
        super();
    }

    public LinkAnnotation (final URI about)
            throws URISyntaxException
    {
        super(about);
    }

    @Override
    @OslcName("creator")
    @OslcPropertyDefinition(BasicAdaptorConstants.DUBLIN_CORE_NAMSPACE + "creator")
    @OslcDescription("Creator or creators of the resource. It is likely that the target resource will be a foaf:Person but that is not necessarily the case.")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcValueType(ValueType.Resource)
    @OslcRange({BasicAdaptorConstants.TYPE_PERSON})
    @OslcReadOnly(false)
    public Link getCreator() {
        return creator;
    }

    @Override
    @OslcName("created")
    @OslcPropertyDefinition(BasicAdaptorConstants.DUBLIN_CORE_NAMSPACE + "created")
    @OslcDescription("Timestamp of resource creation")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcValueType(ValueType.DateTime)
    @OslcReadOnly(false)
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreator(Link creator) {
        this.creator = creator;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }
}
