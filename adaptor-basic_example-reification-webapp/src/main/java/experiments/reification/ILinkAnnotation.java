package experiments.reification;

import org.eclipse.lyo.oslc4j.core.annotation.*;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.ValueType;
import se.kth.md.adaptors.basicexample.BasicAdaptorConstants;

import java.util.Date;

@OslcNamespace("http://annotation.org/nsp#")
@OslcName("LinkAnnotation")
@OslcResourceShape(title = "LinkAnnotation Resource Shape", describes = "http://annotation.org/nsp#LinkAnnotation")
public interface ILinkAnnotation {

    @OslcName("creator")
    @OslcPropertyDefinition(BasicAdaptorConstants.DUBLIN_CORE_NAMSPACE + "creator")
    @OslcDescription("Creator or creators of the resource. It is likely that the target resource will be a foaf:Person but that is not necessarily the case.")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcValueType(ValueType.Resource)
    @OslcRange({BasicAdaptorConstants.TYPE_PERSON})
    @OslcReadOnly(false)
    public Link getCreator();

    @OslcName("created")
    @OslcPropertyDefinition(BasicAdaptorConstants.DUBLIN_CORE_NAMSPACE + "created")
    @OslcDescription("Timestamp of resource creation")
    @OslcOccurs(Occurs.ExactlyOne)
    @OslcValueType(ValueType.DateTime)
    @OslcReadOnly(false)
    public Date getCreated();

    public void setCreator(final Link creator );

    public void setCreated(final Date created );

}
