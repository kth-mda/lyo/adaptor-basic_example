package experiments.reification;

import org.eclipse.lyo.oslc4j.core.annotation.OslcOccurs;
import org.eclipse.lyo.oslc4j.core.annotation.OslcValueType;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.Occurs;
import org.eclipse.lyo.oslc4j.core.model.ValueType;
import org.eclipse.lyo.tools.store.ModelUnmarshallingException;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.StoreFactory;
import org.eclipse.lyo.tools.store.internals.JenaTdbStoreImpl;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.*;

public class ExtendedInMemoryStoreImpl extends JenaTdbStoreImpl {

    private Store store;

    public ExtendedInMemoryStoreImpl () {
        store = StoreFactory.inMemory();
    }

    public <T extends AbstractResource> T getResourceUnderKey(URI key, URI uri, Class<T> clazz, boolean withAnnotations) throws NoSuchElementException, StoreAccessException, ModelUnmarshallingException {

        AbstractResource resource = store.getResourceUnderKey(key, uri, clazz);
        if (withAnnotations) {
            Class c = resource.getClass();
            for (Method m : c.getMethods()) {
                if (isOslcValueTypeResource(m)) {
                    if (isOslcOccursOne(m)) {
                        try {
                            Link link = (Link) m.invoke(resource);
                            if (link.getLabel() != null && !link.getLabel().isEmpty()) {
                                AbstractResource annotation = store.getResourceUnderKey(key, URI.create(link.getLabel()), LinkAnnotation.class);
                                Method setterMethod = getSetterMethod(c, m.getName());

                                ExtendedLink extLink = new ExtendedLink(link.getValue(), annotation.getAbout().toString(), annotation);
                                setterMethod.invoke(extLink);
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            HashSet<Link> links = (HashSet<Link>) m.invoke(resource);
                            HashSet<ExtendedLink> extLinks = new HashSet<>();
                            for (Link link : links) {
                                if (link.getLabel() != null && !link.getLabel().isEmpty()) {
                                    AbstractResource annotation = store.getResourceUnderKey(key, URI.create(link.getLabel()), LinkAnnotation.class);

                                    ExtendedLink extLink = new ExtendedLink(link.getValue(), annotation.getAbout().toString(), annotation);
                                    extLinks.add(extLink);
                                }
                            }
                            Method setterMethod = getSetterMethod(c, m.getName());
                            setterMethod.invoke(resource, extLinks);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return (T)resource;
    }

    private Method getSetterMethod (Class clazz, String getterMethodName) {
        String setterMethodName = "set" + getterMethodName.substring(3);
        for (Method m : clazz.getMethods()) {
            if (m.getName().equals(setterMethodName)) {
                return m;
            }
        }
        return null;
    }

    public <T extends AbstractResource> boolean putResources(URI key, final Collection<T> resources, boolean storeAnnotations) throws StoreAccessException {
        List<AbstractResource> resourcesToProcess = new ArrayList<>();
        if (storeAnnotations) {
            for (AbstractResource aR : resources) {
                Class c = aR.getClass();
                resourcesToProcess.addAll(retrieveAnnotationsFromOslc4JAnnotatedModel(c, aR));
            }
        }
        resourcesToProcess.addAll(resources);

        return store.putResources(key, resourcesToProcess);
    }

    private List<AbstractResource> retrieveAnnotationsFromOslc4JAnnotatedModel (Class c, AbstractResource resource) {
        List<AbstractResource> annotations = new ArrayList<>();
        for (Method m : c.getMethods()) {
            if (isOslcValueTypeResource(m)) {
                if (isOslcOccursOne(m)) {
                    try {
                        Link link = (Link)m.invoke(resource);
                        if (link instanceof ExtendedLink) {
                            ExtendedLink extLink = (ExtendedLink)link;
                            if (extLink.getAnnotation() != null) {
                                annotations.add(extLink.getAnnotation());
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        HashSet<Link> links = (HashSet<Link>)m.invoke(resource);
                        for (Link link : links) {
                            if (link instanceof ExtendedLink) {
                                ExtendedLink extLink = (ExtendedLink)link;
                                if (extLink.getAnnotation() != null) {
                                    annotations.add(extLink.getAnnotation());
                                }
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return annotations;
    }

    private boolean isOslcValueTypeResource (Method m) {
        boolean result = false;
        for (Annotation a : m.getDeclaredAnnotations()) {
            if (a instanceof OslcValueType) {
                if (((OslcValueType)a).value().equals(ValueType.Resource)) {
                    return true;
                }
            }
        }
        return result;
    }

    private boolean isOslcOccursOne (Method m) {
        for (Annotation a : m.getDeclaredAnnotations()) {
            if (a instanceof  OslcOccurs) {
                if (((OslcOccurs)a).value().equals(Occurs.ExactlyOne)
                        || ((OslcOccurs)a).value().equals(Occurs.ZeroOrOne)) {
                    return true;
                } else if (((OslcOccurs)a).value().equals(Occurs.OneOrMany)
                        || ((OslcOccurs)a).value().equals(Occurs.ZeroOrMany)) {
                    return false;
                }
            }
        }
        return false;
    }

}
