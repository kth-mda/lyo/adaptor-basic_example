# Basic Adaptor example

## Structure

* `adaptor-basic_example-model` contains adaptor toolchain model used to generate the resource classes and the webapp
* `adaptor-basic_example-webapp` contains the generated adaptor
* `adaptor-basic_example-webapp-authentication-webapp` for experiments on authentication and access control
* `adaptor-basic_example-webapp-reification-webapp` for experiments on reification
* `adaptor-basic_example-client` for the client side
 
## License

> Copyright 2023 KTH Royal Institute of Technology
> 
> Licensed under the EUPL-1.2-or-later, with extension of article 5 (compatibility clause) to any licence for distributing derivative works that have been produced by the normal use of the Work as a library.

This project uses code from the Eclipse Lyo project dual-licensed under `EPL-1.0 OR BSD-3-Clause`. We chose to use that code under the terms of `EPL-1.0`.

> Copyright (c) 2012 IBM Corporation and Contributors to the Eclipse Foundation.
> 
>  All rights reserved. This program and the accompanying materials
>  are made available under the terms of the Eclipse Public License v1.0
>  and Eclipse Distribution License v. 1.0 which accompanies this distribution.
>  
>  The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
>  and the Eclipse Distribution License is available at
>  http://www.eclipse.org/org/documents/edl-v10.php.
> 
> SPDX-License-Identifier: EPL-1.0 OR BSD-3-Clause