/*******************************************************************************
 * Copyright (c) 2012 IBM Corporation and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v. 1.0 which accompanies this distribution.
 *
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *
 *     Russell Boykin       - initial API and implementation
 *     Alberto Giammaria    - initial API and implementation
 *     Chris Peters         - initial API and implementation
 *     Gianluca Bernardini  - initial API and implementation
 *     Michael Fiedler      - adapted for Bugzilla service provider
 *     Jad El-khoury        - initial implementation of code generator (https://bugs.eclipse.org/bugs/show_bug.cgi?id=422448)
 *     Matthieu Helleboid   - initialize each service provider separately
 *     Anass Radouani       - initialize each service provider separately
 *
 * This file is generated by org.eclipse.lyo.oslc4j.codegenerator
 *******************************************************************************/

package se.kth.md.adaptors.basicexample.servlet;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.eclipse.lyo.oslc4j.client.ServiceProviderRegistryURIs;
import org.eclipse.lyo.oslc4j.core.model.Publisher;
import org.eclipse.lyo.oslc4j.core.model.Service;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.oslc4j.core.model.ServiceProviderCatalog;

import se.kth.md.adaptors.basicexample.BasicAdaptorManager;
import se.kth.md.adaptors.basicexample.BdServicesServiceProviderInfo;
import se.kth.md.adaptors.basicexample.ReqServicesServiceProviderInfo;

// Start of user code imports
// End of user code

/**
 * This is the OSLC service provider catalog for the Bugzilla adapter.  Service providers are
 * not registered with the catalog until a request comes in to access either the catalog or a
 * specific service provider.   This request could be from an external consumer or an internal
 * request triggered by a consumer accessing a change request.
 *
 * The service providers are created and registered in the initServiceProvidersFromProducts()
 * method.  A list of accessible products is retrieved from Bugzilla and a ServiceProvider is
 * created and registered for each using the Bugzilla productId as the identifier.
 *
 * The registered service providers are refreshed on each catalog or service provider collection
 * request.
 */
public class ServiceProviderCatalogSingleton
{
    private static final ServiceProviderCatalog             serviceProviderCatalog;
    private static final SortedMap<String, ServiceProvider> serviceProviders = new TreeMap<String, ServiceProvider>();

    static
    {
        serviceProviderCatalog = new ServiceProviderCatalog();
        URI catalogUri = UriBuilder.fromUri(ServletListener.getServicesBase()).path("/catalog/singleton").build();
        serviceProviderCatalog.setAbout(catalogUri);
        serviceProviderCatalog.setTitle("Basic Example Service Provider Catalog");
        serviceProviderCatalog.setDescription("Service Provider Catalog");
    }

    private ServiceProviderCatalogSingleton()
    {
        super();
    }


    public static URI getUri()
    {
        return serviceProviderCatalog.getAbout();
    }

    public static ServiceProviderCatalog getServiceProviderCatalog(HttpServletRequest httpServletRequest)
    {
        initServiceProviders(httpServletRequest);
        return serviceProviderCatalog;
    }

    public static ServiceProvider [] getServiceProviders(HttpServletRequest httpServletRequest)
    {
        synchronized(serviceProviders)
        {
            initServiceProviders(httpServletRequest);
            return serviceProviders.values().toArray(new ServiceProvider[ serviceProviders.size()]);
        }
    }


    public static URI constructBdServicesServiceProviderURI(final String serviceProviderId)
    {
        String basePath = ServletListener.getServicesBase();
        Map<String, Object> pathParameters = new HashMap<String, Object>();
        pathParameters.put("serviceProviderId", serviceProviderId);
        String instanceURI = "bd-services/{serviceProviderId}";

        final UriBuilder builder = UriBuilder.fromUri(basePath);
        return builder.path(instanceURI).buildFromMap(pathParameters);
    }

    private static String bdServicesServiceProviderIdentifier(final String serviceProviderId)
    {
        String identifier = "/" + serviceProviderId;
        return identifier;
    }

    public static ServiceProvider getBdServicesServiceProvider(HttpServletRequest httpServletRequest, final String serviceProviderId)
    {
        ServiceProvider serviceProvider;

        synchronized(serviceProviders)
        {
            String identifier = bdServicesServiceProviderIdentifier(serviceProviderId);
            serviceProvider = serviceProviders.get(identifier);

            //One retry refreshing the service providers
            if (serviceProvider == null)
            {
                getServiceProviders(httpServletRequest);
                serviceProvider = serviceProviders.get(identifier);
            }
        }

        if (serviceProvider != null)
        {
            return serviceProvider;
        }

        throw new WebApplicationException(Status.NOT_FOUND);
    }

    public static ServiceProvider registerBdServicesServiceProvider(final HttpServletRequest httpServletRequest,
                                                          final ServiceProvider serviceProvider,
                                                          final String serviceProviderId)
                                                throws URISyntaxException
    {
        synchronized(serviceProviders)
        {
            final URI serviceProviderURI = constructBdServicesServiceProviderURI(serviceProviderId);
            return registerBdServicesServiceProviderNoSync(serviceProviderURI,
                                                 serviceProvider,
                                                 serviceProviderId);
        }
    }

    /**
    * Register a service provider with the OSLC catalog
    *
    * @param serviceProviderURI
    * @param serviceProvider
    * @param productId
    * @return
    */
    private static ServiceProvider registerBdServicesServiceProviderNoSync(final URI serviceProviderURI,
                                                                 final ServiceProvider serviceProvider
                                                                 , final String serviceProviderId)
    {
        final SortedSet<URI> serviceProviderDomains = getServiceProviderDomains(serviceProvider);

        String identifier = bdServicesServiceProviderIdentifier(serviceProviderId);
        serviceProvider.setAbout(serviceProviderURI);
        serviceProvider.setIdentifier(identifier);
        serviceProvider.setCreated(new Date());
        serviceProvider.setDetails(new URI[] {serviceProviderURI});

        serviceProviderCatalog.addServiceProvider(serviceProvider);
        serviceProviderCatalog.addDomains(serviceProviderDomains);

        serviceProviders.put(identifier, serviceProvider);

        return serviceProvider;
    }

    // This version is for self-registration and thus package-protected
    static ServiceProvider registerBdServicesServiceProvider(final ServiceProvider serviceProvider, final String serviceProviderId)
                                            throws URISyntaxException
    {
        synchronized(serviceProviders)
        {
            final URI serviceProviderURI = constructBdServicesServiceProviderURI(serviceProviderId);

            return registerBdServicesServiceProviderNoSync(serviceProviderURI, serviceProvider, serviceProviderId);
        }
    }

    public static void deregisterBdServicesServiceProvider(final String serviceProviderId)
    {
        synchronized(serviceProviders)
        {
            final ServiceProvider deregisteredServiceProvider = serviceProviders.remove(bdServicesServiceProviderIdentifier(serviceProviderId));

            if (deregisteredServiceProvider != null)
            {
                final SortedSet<URI> remainingDomains = new TreeSet<URI>();

                for (final ServiceProvider remainingServiceProvider : serviceProviders.values())
                {
                    remainingDomains.addAll(getServiceProviderDomains(remainingServiceProvider));
                }

                final SortedSet<URI> removedServiceProviderDomains = getServiceProviderDomains(deregisteredServiceProvider);

                removedServiceProviderDomains.removeAll(remainingDomains);
                serviceProviderCatalog.removeDomains(removedServiceProviderDomains);
                serviceProviderCatalog.removeServiceProvider(deregisteredServiceProvider);
            }
            else
            {
                throw new WebApplicationException(Status.NOT_FOUND);
            }
        }
    }

    public static URI constructReqServicesServiceProviderURI(final String serviceProviderId)
    {
        String basePath = ServletListener.getServicesBase();
        Map<String, Object> pathParameters = new HashMap<String, Object>();
        pathParameters.put("serviceProviderId", serviceProviderId);
        String instanceURI = "req-services/{serviceProviderId}";

        final UriBuilder builder = UriBuilder.fromUri(basePath);
        return builder.path(instanceURI).buildFromMap(pathParameters);
    }

    private static String reqServicesServiceProviderIdentifier(final String serviceProviderId)
    {
        String identifier = "/" + serviceProviderId;
        return identifier;
    }

    public static ServiceProvider getReqServicesServiceProvider(HttpServletRequest httpServletRequest, final String serviceProviderId)
    {
        ServiceProvider serviceProvider;

        synchronized(serviceProviders)
        {
            String identifier = reqServicesServiceProviderIdentifier(serviceProviderId);
            serviceProvider = serviceProviders.get(identifier);

            //One retry refreshing the service providers
            if (serviceProvider == null)
            {
                getServiceProviders(httpServletRequest);
                serviceProvider = serviceProviders.get(identifier);
            }
        }

        if (serviceProvider != null)
        {
            return serviceProvider;
        }

        throw new WebApplicationException(Status.NOT_FOUND);
    }

    public static ServiceProvider registerReqServicesServiceProvider(final HttpServletRequest httpServletRequest,
                                                          final ServiceProvider serviceProvider,
                                                          final String serviceProviderId)
                                                throws URISyntaxException
    {
        synchronized(serviceProviders)
        {
            final URI serviceProviderURI = constructReqServicesServiceProviderURI(serviceProviderId);
            return registerReqServicesServiceProviderNoSync(serviceProviderURI,
                                                 serviceProvider,
                                                 serviceProviderId);
        }
    }

    /**
    * Register a service provider with the OSLC catalog
    *
    * @param serviceProviderURI
    * @param serviceProvider
    * @param productId
    * @return
    */
    private static ServiceProvider registerReqServicesServiceProviderNoSync(final URI serviceProviderURI,
                                                                 final ServiceProvider serviceProvider
                                                                 , final String serviceProviderId)
    {
        final SortedSet<URI> serviceProviderDomains = getServiceProviderDomains(serviceProvider);

        String identifier = reqServicesServiceProviderIdentifier(serviceProviderId);
        serviceProvider.setAbout(serviceProviderURI);
        serviceProvider.setIdentifier(identifier);
        serviceProvider.setCreated(new Date());
        serviceProvider.setDetails(new URI[] {serviceProviderURI});

        serviceProviderCatalog.addServiceProvider(serviceProvider);
        serviceProviderCatalog.addDomains(serviceProviderDomains);

        serviceProviders.put(identifier, serviceProvider);

        return serviceProvider;
    }

    // This version is for self-registration and thus package-protected
    static ServiceProvider registerReqServicesServiceProvider(final ServiceProvider serviceProvider, final String serviceProviderId)
                                            throws URISyntaxException
    {
        synchronized(serviceProviders)
        {
            final URI serviceProviderURI = constructReqServicesServiceProviderURI(serviceProviderId);

            return registerReqServicesServiceProviderNoSync(serviceProviderURI, serviceProvider, serviceProviderId);
        }
    }

    public static void deregisterReqServicesServiceProvider(final String serviceProviderId)
    {
        synchronized(serviceProviders)
        {
            final ServiceProvider deregisteredServiceProvider = serviceProviders.remove(reqServicesServiceProviderIdentifier(serviceProviderId));

            if (deregisteredServiceProvider != null)
            {
                final SortedSet<URI> remainingDomains = new TreeSet<URI>();

                for (final ServiceProvider remainingServiceProvider : serviceProviders.values())
                {
                    remainingDomains.addAll(getServiceProviderDomains(remainingServiceProvider));
                }

                final SortedSet<URI> removedServiceProviderDomains = getServiceProviderDomains(deregisteredServiceProvider);

                removedServiceProviderDomains.removeAll(remainingDomains);
                serviceProviderCatalog.removeDomains(removedServiceProviderDomains);
                serviceProviderCatalog.removeServiceProvider(deregisteredServiceProvider);
            }
            else
            {
                throw new WebApplicationException(Status.NOT_FOUND);
            }
        }
    }

    private static SortedSet<URI> getServiceProviderDomains(final ServiceProvider serviceProvider)
    {
        final SortedSet<URI> domains = new TreeSet<URI>();

        if (serviceProvider!=null) {
            final Service [] services = serviceProvider.getServices();
            for (final Service service : services)
            {
                final URI domain = service.getDomain();

                domains.add(domain);
            }
        }
        return domains;
    }

    /**
     * Retrieve a list of products from Bugzilla and construct a service provider for each.
     *
     * Each product ID is added to the parameter map which will be used during service provider
     * creation to create unique URI paths for each Bugzilla product.  See @Path definition at
     * the top of BugzillaChangeRequestService.
     *
     * @param httpServletRequest
     */
    protected static void initServiceProviders (HttpServletRequest httpServletRequest)
    {
        try {
            // Start of user code initServiceProviders
            // End of user code

            String basePath = ServletListener.getServicesBase();
            
            BdServicesServiceProviderInfo [] bdServicesServiceProviderInfos = BasicAdaptorManager.getBdServicesServiceProviderInfos(httpServletRequest);
            //Register each service provider
            for (BdServicesServiceProviderInfo serviceProviderInfo : bdServicesServiceProviderInfos) {
                String identifier = bdServicesServiceProviderIdentifier(serviceProviderInfo.serviceProviderId);
                if (! serviceProviders.containsKey(identifier)) {
                    String serviceProviderName = serviceProviderInfo.name;
                    String title = "Basic Domain Service Provider: " + "(" + identifier + ")";
                    String description = "Service Provider for Basic Domain Resources: " + serviceProviderName + "(" + identifier + ")";
                    Publisher publisher = null;
                    Map<String, Object> parameterMap = new HashMap<String, Object>();
                    parameterMap.put("serviceProviderId", serviceProviderInfo.serviceProviderId);
                    final ServiceProvider aServiceProvider = BdServicesServiceProvidersFactory.createServiceProvider(basePath, title, description, publisher, parameterMap);
                    registerBdServicesServiceProvider(aServiceProvider, serviceProviderInfo.serviceProviderId);
                }
            }
            
            ReqServicesServiceProviderInfo [] reqServicesServiceProviderInfos = BasicAdaptorManager.getReqServicesServiceProviderInfos(httpServletRequest);
            //Register each service provider
            for (ReqServicesServiceProviderInfo serviceProviderInfo : reqServicesServiceProviderInfos) {
                String identifier = reqServicesServiceProviderIdentifier(serviceProviderInfo.serviceProviderId);
                if (! serviceProviders.containsKey(identifier)) {
                    String serviceProviderName = serviceProviderInfo.name;
                    String title = "Extended Requirement Service Provider: " + "(" + identifier + ")";
                    String description = "Service Provider for Extended Requirements: " + serviceProviderName + "(" + identifier + ")";
                    Publisher publisher = null;
                    Map<String, Object> parameterMap = new HashMap<String, Object>();
                    parameterMap.put("serviceProviderId", serviceProviderInfo.serviceProviderId);
                    final ServiceProvider aServiceProvider = ReqServicesServiceProvidersFactory.createServiceProvider(basePath, title, description, publisher, parameterMap);
                    registerReqServicesServiceProvider(aServiceProvider, serviceProviderInfo.serviceProviderId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(e,Status.INTERNAL_SERVER_ERROR);
        }
    }
}

